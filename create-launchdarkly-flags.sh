#!/bin/bash -e

# LAUNCHDARKLY_ACCESS_TOKEN         (Required) API Access token for your LaunchDarkly account (https://app.launchdarkly.com/settings#/tokens) 
# LAUNCHDARKLY_PROJECT_KEY          (Required) LaunchDarkly project key

filename='ld-flags.conf'

if [ -f "$filename" ]
then
  while read f; do
    body="{\"name\":\"$f\",\"key\":\"$f\", \"variations\":[{\"value\":true},{\"value\":false}]}"
    echo "Creating feature flag $f in project $LAUNCHDARKLY_PROJECT_KEY with body $body"
    curl -g \
      --request POST \
      -H "User-Agent: launchdarky/bitbucket-pipelines" \
      -H "Content-type: application/json" \
      -H "Authorization: ${LAUNCHDARKLY_ACCESS_TOKEN}" \
      --data "$body" \
      https://app.launchdarkly.com/api/v2/flags/${LAUNCHDARKLY_PROJECT_KEY}
    echo ""    
  done < $filename
fi