Using LaunchDarkly with Bitbucket Pipelines
-------------------------------------------

This repository contains example scripts demonstrating how to integrate LaunchDarkly with Bitbucket Pipelines.

At the moment, we provide two scripts that you can add to your pipelines:

* Create feature flags in your Pipelines build (`create-launchdarkly-flags.sh`)

    This script lets you create feature flags within a specified project. The feature flag will be created in all environments for that project.

* Enable a feature flag in your Pipelines build (`enable-launchdarkly-flag.sh`)

    This script lets you turn on a specific feature flag for a specific environment within a project.


For more details, refer to our [documentation](http://docs.launchdarkly.com/v2.0/docs/bitbucket-pipelines).